import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public isOpen: boolean = false;
  public showButton: string = ' Show order summary';
  constructor() { }

  ngOnInit() {
  }

  toggle() {
    this.isOpen = !this.isOpen;
    this.showButton = this.isOpen ? ' Hide order summary' : ' Show order summary';
  }

}
