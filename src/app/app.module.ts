import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

import { AppComponent } from './app.component';
import { CustomerInformationComponent } from './customer-information/customer-information.component';
import { ShippingMethodComponent } from './shipping-method/shipping-method.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { BreadcrumbComponent } from './shared/breadcrumb/breadcrumb.component';
import { BasketComponent } from './shared/basket/basket.component';
import { FooterComponent } from './shared/footer/footer.component';

import { AccordionModule } from './shared/accordion/accordion.module';
import { AppRoutingModule } from './app.routing';
import { HeaderComponent } from './shared/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomerInformationComponent,
    ShippingMethodComponent,
    PaymentMethodComponent,
    BreadcrumbComponent,
    BasketComponent,
    FooterComponent,
    HeaderComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  exports: [BreadcrumbComponent, BasketComponent, FooterComponent, HeaderComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AccordionModule,
    AppRoutingModule,
    FormsModule, ReactiveFormsModule,
    TextMaskModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
