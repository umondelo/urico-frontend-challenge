import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-information',
  templateUrl: './customer-information.component.html',
  styleUrls: ['./customer-information.component.scss']
})
export class CustomerInformationComponent implements OnInit {
  public customerForm: FormGroup;

  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm() {
    this.customerForm = this.fb.group({
      email: ['', Validators.required],
      keep: '',
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      company: [''],
      address: ['', Validators.required],
      apartment: '',
      city: '',
      country: '',
      postal: '',
      phone: ['', Validators.required],
      saveInfo: ''
    })

  }

  continue() {
    console.log('submit', this.customerForm.valid)
    if (this.customerForm.valid) {
      console.log(this.customerForm.value)
      this.router.navigate(['shipping-method']);
    } else {
      this.validateForm();
    }
  }

  private validateForm() {
    Object.keys(this.customerForm.controls).forEach(field => {
      const control = this.customerForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

}
