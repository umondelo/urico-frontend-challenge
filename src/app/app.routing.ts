import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CustomerInformationComponent } from './customer-information/customer-information.component';
import { ShippingMethodComponent } from './shipping-method/shipping-method.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/customer-information',
    pathMatch: 'full'
  },
  // {
  //   path: '**',
  //   redirectTo: '/customer-information'
  // },
  {
    path: 'customer-information',
    component: CustomerInformationComponent
  },
  {
    path: 'shipping-method',
    component: ShippingMethodComponent
  },
  {
    path: 'payment-method',
    component: PaymentMethodComponent
  }
];

export const AppRoutingModule = RouterModule.forRoot(appRoutes, {
    useHash: true
});
