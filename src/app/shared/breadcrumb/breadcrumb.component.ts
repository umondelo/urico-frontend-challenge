import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  public links = [
    {
      url: '/customer-information',
      label: 'Customer Information'
    },
    {
      url: '/shipping-method',
      label: 'Shipping Method'
    },
    {
      url: '/payment-method',
      label: 'Payment-method'
    }
  ];
  constructor(private router: Router) { }

  ngOnInit() {
  }

}
