import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-accordion-panel',
  templateUrl: './accordion-panel.component.html',
  styleUrls: ['./accordion-panel.component.scss']
})
export class AccordionPanelComponent implements OnInit {
  @Input() public title: string;
  @Input() public name: string;
  @Input() public desc: string;
  @Input() public imgSrc: string;
  @Input() public isCC: boolean = false;
  @Output() public toggleAccordion: EventEmitter<any> = new EventEmitter<any>();
  @Input() public show: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  toggle() {
    this.toggleAccordion.emit();
  }

}
