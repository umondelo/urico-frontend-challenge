import { Component, ContentChildren, QueryList, AfterContentInit, OnDestroy } from '@angular/core';
import { AccordionPanelComponent } from './accordion-panel/accordion-panel.component';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements AfterContentInit, OnDestroy {
  @ContentChildren(AccordionPanelComponent) panels: QueryList<AccordionPanelComponent>;
  private accordionAlive: boolean = true;

  constructor() { }

  ngAfterContentInit() {
      // Loop through all panels
      this.panels.toArray().forEach(panel => {
          // subscribe panel toggle event
          panel.toggleAccordion.subscribe(() => {
              // Open the panel
              this.openPanel(panel);
          });
      });
  }

  public ngOnDestroy() {
      // this.accordionAlive = false;
  }

  openPanel(panel: AccordionPanelComponent) {
      // close all panels
      this.panels.toArray().forEach(p => (p.show = false));
      // open the selected panel
      panel.show = true;
  }


}
