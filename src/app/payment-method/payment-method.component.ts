import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.scss']
})
export class PaymentMethodComponent implements OnInit {
  public paymentForm: FormGroup;
  public error: string;

  public cardNumber = [
        /[0-9]/,
        /[0-9]/,
        /[0-9]/,
        /[0-9]/,
        '-',
        /[0-9]/,
        /[0-9]/,
        /[0-9]/,
        /[0-9]/,
        '-',
        /[0-9]/,
        /[0-9]/,
        /[0-9]/,
        /[0-9]/,
        '-',
        /[0-9]/,
        /[0-9]/,
        /[0-9]/,
        /[0-9]/
    ];

  public expiration = [
      /[0-1]/,
      /[0-9]/,
      '/',
      /[2]/,
      /[0-9]/,
  ]

  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm() {
    this.paymentForm = this.fb.group({
      cardNumber: ['', [Validators.minLength(16), Validators.required]],
      name: ['', Validators.required],
      date: ['', Validators.required],
      cvv: ['', [Validators.minLength(16), Validators.required]]
    })
  }

  private validateForm() {
    Object.keys(this.paymentForm.controls).forEach(field => {
      const control = this.paymentForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

  submitForm() {
    if (this.paymentForm.valid) {
      console.log(this.paymentForm.value)
    } else {
      this.validateForm();
      this.error = "The information you provided couldn't be verified. Please Check your card details and try again."
    }
  }

}
