import { Component, OnInit, Renderer2, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shipping-method',
  templateUrl: './shipping-method.component.html',
  styleUrls: ['./shipping-method.component.scss']
})
export class ShippingMethodComponent implements OnInit {

  constructor(private renderer: Renderer2, private router: Router) {
    this.renderer.addClass(document.body, 'shipping');
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'shipping');
  }

  continue() {
    this.router.navigate(['payment-method']);
  }

}
